﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using RPGCore.Grid;
using RPGCore;

namespace RPGCore.Interfaces
{
    public interface ISelectable
    {
        GameObject select();
        Vector3 pointat();
        bool InRange(Coordinate c, int r);
        void click();
    }
    public delegate bool EndChase(IChasable target);
    public interface IChasable
    {
        event EndChase end;
    }
    public interface IWatchable
    {
        Vector3 Watchpoint();
    }
    
    public interface IField : ISelectable
    {
        bool setFrame();
        bool setFrame(Color c);
        Field me();
        void deselect();
    }
    
}
