﻿using System;
using System.Collections.Generic;
using UnityEngine;
using RPGCore;
using RPGCore.Grid;

namespace RPGCore.Interfaces
{
    public interface ITargetable : ISelectable
    {
        bool apply();
    }
    public interface IControllable : IMove
    {
        bool myTurn();
    }
    public interface IMove
    {
        Field moveTo(Field f);
        Field teleportTo(Field f);
        Field current();
        int Range();
    }
    public interface ITurn
    {
        void activate();
        float speed();
        void register();
        void endTurn();
        Field current();
    }
    public interface ITick
    {
        bool decay();
    }
}
