﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using RPGCore.Interfaces;
namespace RPGCore.Vision
{
    class CameraBehaviour : MonoBehaviour
    {
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                    Debug.Log("Clicked on:" + hit.transform.name);
                    ISelectable target = hit.transform.GetComponent<ISelectable>();
                    if(target != null)
                    {
                        target.click();
                    }
                }
            }
        }
    }
}
