﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPGCore.Interfaces;
using RPGCore.Turn;
namespace RPGCore.Vision
{
    public class VisionManager : MonoBehaviour
    {

        // Use this for initialization
        [SerializeField]
        private CameraPointer Pointer;
        public Camera current()
        {
            return Pointer.cam;
        }
        void Start()
        {

        }
        public void Switch(CameraPointer p)
        {
            Pointer = p;
        }
        // Update is called once per frame
        public void FollowMe(IChasable c)
        {
            Pointer.Chase(c);
        }
    }
}

