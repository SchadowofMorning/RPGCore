﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPGCore.Interfaces;
using RPGCore.Turn;

namespace RPGCore.Vision
{
    public class CameraPointer : MonoBehaviour
    {
        TurnManager turnmanager;
        // Use this for initialization
        ISelectable target;
        bool unlock = false;
        bool chase = false;
        public Camera cam;

        void Start()
        {
            turnmanager = GameObject.Find("TurnManager").GetComponent<TurnManager>();
            cam = transform.Find("Camera").GetComponent<Camera>();
        }
        public bool trigger() { return unlock = !unlock; }
        public bool Chase(IChasable tar)
        {
            if (chase)
            {
                tar.end -= Chase;
            } else
            {
                tar.end += Chase;
            }
            return chase = !chase;
        }
        
        void Update()
        {
            if (Input.GetMouseButton(1))
            {
                transform.Rotate(Vector3.up, Input.GetAxis("Mouse X") * 2);
            }
        }
        // Update is called once per frame
        void FixedUpdate()
        {
            target = turnmanager.current as ISelectable;
            if(target is ISelectable)
            {
                if (unlock == false && chase == false)
                {
                    transform.position = Vector3.Lerp(transform.position, target.pointat(), 0.1f);
                }
                if (chase == true)
                {
                    transform.position = Vector3.Lerp(transform.position, target.pointat(), 0.1f);
                }
                else if (unlock == true)
                {

                }
            }
            
        }
    }
}

