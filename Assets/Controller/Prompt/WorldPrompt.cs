﻿using System;
using System.Collections.Generic;
using System.Linq;
using RPGCore.Grid;
using UnityEngine;
using UnityEngine.UI;
using RPGCore.Turn;
using RPGCore.Interfaces;
using RPGCore.Vision;
namespace RPGCore.Prompt
{
    public class WorldPrompt : MonoBehaviour
    {
        VisionManager vm;
        TurnManager tm;
        Camera cam;
        CanvasGroup cr;
        IField f;
        /// <summary>
        /// Calling the Prompt from static function, can be used by every Object that implements ISelectable
        /// TODO Implement Interfaces for Shops, Enemys etcetera.
        /// </summary>
        /// <param name="v"></param>
        /// <param name="selector"></param>
        public static void ShowAt(Vector3 v, ISelectable selector)
        {
            Debug.Log("Calling Prompt");
            WorldPrompt wp = GameObject.Find("WorldPrompt").GetComponent<WorldPrompt>();
            wp.transform.position = v;
            wp.ShowPrompt();
            if (selector is IField)
            {
                if (wp.f != null)  wp.f.deselect(); //Deselect the last field if there is one
                wp.f = selector as IField;
            }
        }
        public void ShowPrompt()
        {
            if(tm.current is IControllable)cr.alpha = 1;
        }
        public void HidePrompt()
        {
            cr.alpha = 0;
        }
        /// <summary>
        /// Invoked by the Button on the prompt
        /// </summary>
        public void MoveClick()
        {
            IControllable ch = tm.current as IControllable;
            Field startpoint = ch.current();
            startpoint.setFrame();
            Coordinate c1 = startpoint.position;
            Coordinate c2 = f.me().position;
            if(c2 >= c1 - ch.Range() && c2 <= c1 + ch.Range())
            {
                ch.moveTo(f.me());
                HidePrompt();
            }
        }
        void Start()
        {
            cr = this.GetComponent<CanvasGroup>();
            HidePrompt();
            vm = GameObject.Find("VisionManager").GetComponent<VisionManager>();
            tm = GameObject.Find("TurnManager").GetComponent<TurnManager>();
        }
        /// <summary>
        /// Face the Camera
        /// </summary>
        void Update()
        {
            transform.LookAt(vm.current().transform, Vector3.up);
        }
    }
}
