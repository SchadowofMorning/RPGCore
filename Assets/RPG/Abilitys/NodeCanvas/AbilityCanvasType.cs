﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeEditorFramework;
using NodeEditorFramework.Standard;
using RPGCore.WorldObjects;
using RPGCore.Grid;

namespace RPGCore.Abilitys.Nodes
{
    [NodeCanvasType("Ability Canvas")]
    public class AbilityCanvasType : NodeCanvas
    {
        public override string canvasName { get {  return "Ability Canvas";  } }
        protected override void OnCreate()
        {
            Traversal = new CanvasCalculator(this);
        }
        private bool Valid(out Type t)
        {
            List<Node> targetinvoker = nodes.FindAll(x => (x is TargetInvokeNode));
            List<Node> invoker = nodes.FindAll(x => (x is InvokeNode));
            if(invoker.Count == 1 ^ targetinvoker.Count == 1)
            {
                if(invoker.Count == 1)
                {
                    t = typeof(InvokeNode);
                } else
                {
                    t = typeof(TargetInvokeNode);
                }
                return true;
            } else
            {
                t = null;
                return false;
            }
        }
        public void Invoke(Character c, Coordinate coord)
        {
            Type invokerType;
            if (!Valid(out invokerType)) return;
            Node invoker = nodes.Find(x => (x is InvokeNode));
            List<Node> targeter = nodes.FindAll(x => (x is TargetNode));
            List<Node> effects = nodes.FindAll(x => (x is EffectBaseNode));
            ((InvokeNode)invoker).Invokation(c, coord);
            TraverseAll();
            foreach (Node t in targeter)
            {
                TargetNode target = (TargetNode)t;
                target.getTargets();
            }
            foreach(Node e in effects)
            {
                EffectBaseNode effect = (EffectBaseNode)e;
                effect.Apply();
            }
        }

        public void Invoke(Character c, List<Character> targets) {
            Type invokerType;
            if (!Valid(out invokerType)) return;
            Node invoker = nodes.Find(x => (x is TargetInvokeNode));
            List<Node> effects = nodes.FindAll(x => (x is EffectBaseNode));
            ((TargetInvokeNode)invoker).Invokation(c, targets);
            TraverseAll();
            foreach (Node e in effects)
            {
                EffectBaseNode effect = (EffectBaseNode)e;
                effect.Apply();
            }
        }
    }
}
