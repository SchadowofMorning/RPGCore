﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using RPGCore.WorldObjects;
using NodeEditorFramework;
using NodeEditorFramework.Utilities;

namespace RPGCore.Abilitys.Nodes
{
    [Node(false, "Converter/Character")]
    public class CharacterConverterNode : AbilityBaseNode
    {
        const string NodeID = "CharacterConverterNode";
        public override string GetID
        {
            get
            {
                return NodeID;
            }
        }
        public override string Title
        {
            get
            {
                return "Character Converter";
            }
        }
        public override Vector2 MinSize
        {
            get
            {
                return new Vector2(200, 50);
            }
        }
        enum chartype { SingleToList, ListToSingle };
        int type = 0;
        chartype conversion;
        

        bool swapper = false;
        public override void NodeGUI()
        {
            ValueConnectionKnobAttribute AtInputChar = new ValueConnectionKnobAttribute("connector", Direction.In, typeof(Character));
            ValueConnectionKnobAttribute AtInputList = new ValueConnectionKnobAttribute("connector", Direction.In, typeof(List<Character>));
            ValueConnectionKnobAttribute AtOutputChar = new ValueConnectionKnobAttribute("connector", Direction.Out, typeof(Character));
            ValueConnectionKnobAttribute AtOutputList = new ValueConnectionKnobAttribute("connector", Direction.Out, typeof(List<Character>));
            base.NodeGUI();
            if (GUILayout.Button("Swap"))
            {
                swapper = !swapper;
            }
            if(dynamicConnectionPorts.Count != 2)
            {
                CreateValueConnectionKnob(AtInputChar);
                CreateValueConnectionKnob(AtOutputList);
            }
            if (!matches())
            {
                dynamicConnectionPorts.Clear();
                if (swapper)
                {
                    CreateValueConnectionKnob(AtInputList);
                    CreateValueConnectionKnob(AtOutputChar);
                } else
                {
                    CreateValueConnectionKnob(AtInputChar);
                    CreateValueConnectionKnob(AtOutputList);
                }
            }
        }
        private bool matches() {
            ValueConnectionKnob input = (ValueConnectionKnob)dynamicConnectionPorts[0];
            ValueConnectionKnob output = (ValueConnectionKnob)dynamicConnectionPorts[1];
            if (swapper)
            {
                if (output.valueType == typeof(List<Character>)) return false;
                if (input.valueType == typeof(Character)) return false;
                return true;
            } else
            {
                if (input.valueType == typeof(List<Character>)) return false;
                if (output.valueType == typeof(Character)) return false;
                return true;
            }
        }
    }
}
