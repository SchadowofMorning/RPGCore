﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeEditorFramework;
using RPGCore.WorldObjects;
using RPGCore.Abilitys.Nodes;
using UnityEditor;
using UnityEngine;
using RPGCore.Traits;

namespace RPGCore.Abilitys.Nodes
{
    [Node(false, "Targeted Invoke Node", new Type[]{typeof(AbilityCanvasType)})]
    public class TargetInvokeNode : AbilityBaseNode
    {
        const string NodeID = "TargetInvokeNode";
        public override string GetID
        {
            get
            {
                return NodeID;
            }
        }
        public override string Title
        {
            get
            {
                return "Targeted Invoke Node";
            }
        }
        [ValueConnectionKnob("Invoker", Direction.Out, typeof(Character))]
        public ValueConnectionKnob casterOutput;
        [ValueConnectionKnob("Targets", Direction.Out, typeof(List<Character>))]
        public ValueConnectionKnob targetOutput;
        Character caster;
        List<Character> targets;
        Dictionary<Trait, int> traits = new Dictionary<Trait, int>();
        public override void NodeGUI()
        {
            traits.Clear();
            foreach (Trait t in Enum.GetValues(typeof(Trait)))
            {
                traits.Add(t, 1);
            }
            GUILayout.BeginHorizontal();
            if (dynamicConnectionPorts.Count != traits.Count) //Redraw Values only if they arent the same
            {
                dynamicConnectionPorts.Clear();
                foreach (Trait t in traits.Keys)
                {
                    ValueConnectionKnobAttribute tattr = new ValueConnectionKnobAttribute(t.ToString(), Direction.Out, typeof(float));
                    CreateValueConnectionKnob(tattr);

                }
            }
            GUILayout.EndHorizontal();
            foreach (ValueConnectionKnob t in dynamicConnectionPorts)
            {
                t.DisplayLayout();
            }
            targetOutput.DisplayLayout();
            casterOutput.DisplayLayout();
        }
        public override bool Calculate()
        {
            if (caster != null && targets != null)
            {
                traits = caster.traits;
            }
            else
            {
                targets = new List<Character>();
            }
            if (caster == null)
            {
                casterOutput.SetValue(null);
            }
            else
            {
                casterOutput.SetValue(caster);
            }
            foreach (Trait t in traits.Keys)
            {
                ValueConnectionKnob k = (ValueConnectionKnob)dynamicConnectionPorts.Find(x => (x.name == t.ToString()));

                k.SetValue(traits[t]);
            }
            return true;
        }
        public void Invokation(Character character, List<Character> ts)
        {
            caster = character;
            targets = ts;
        }
    }

}
