﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeEditorFramework;
using RPGCore.Grid;
using UnityEditor;
using RPGCore.WorldObjects;
namespace RPGCore.Abilitys.Nodes
{
    [Node(false, "Ability/Standard Targeting Node")]
    class TargetNode : AbilityBaseNode
    {
        const string NodeID = "TargetNode";
        public override string GetID { get { return NodeID; } }
        public override string Title { get { return "Ability Target"; } }
        public override bool AutoLayout
        {
            get
            {
                return true;
            }
        }
        private Coordinate modifier;
        private int radius;
        private Coordinate val;
        List<Character> targets;
        //Connection Knobs
        //Inputs
        [ValueConnectionKnob("Coordinate", Direction.In, typeof(Coordinate))]
        public ValueConnectionKnob coordinateInput;
        [ValueConnectionKnob("Radius", Direction.In, typeof(float))]
        public ValueConnectionKnob radiusInput;
        //Outputs
        [ValueConnectionKnob("Targets", Direction.Out, typeof(List<Character>))]
        public ValueConnectionKnob targetsOutput;
        public override bool Calculate()
        {
            if (coordinateInput.connected()) modifier = coordinateInput.GetValue<Coordinate>();
            if (radiusInput.connected()) radius = radiusInput.GetValue<int>();
            if(radiusInput.connected() && coordinateInput.connected())
            {
                val = coordinateInput.GetValue<Coordinate>() + modifier;
                targetsOutput.SetValue(targets);
                return true;
            }
            return true;
        }
        public bool getTargets()
        {
            targets.Clear();
            List<Field> worklist = FieldIndex.getAllOccupied();
            foreach(Field f in worklist)
            {
                if(f.position <= val + radius && f.position >= val - radius)
                {
                    targets.Add(f.occupant);
                }
            }
            return true;
        }
    }
}
