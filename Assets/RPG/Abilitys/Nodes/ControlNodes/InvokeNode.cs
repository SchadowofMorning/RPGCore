﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeEditorFramework;
using RPGCore.WorldObjects;
using RPGCore.Grid;
using RPGCore.Traits;
using UnityEditor;
using UnityEngine;
using RPGCore.Interfaces;
namespace RPGCore.Abilitys.Nodes
{
    [Node(false, "Ability/InvokeNode")]
    public class InvokeNode : AbilityBaseNode
    {
        const string NodeID = "Invoke";
        public override string GetID { get { return NodeID; } }
        public override string Title { get { return "Ability Invoke"; } }

        //Connections
        [ValueConnectionKnob("Target coord", Direction.Out, typeof(Coordinate))]
        public ValueConnectionKnob targetpoint;
        [ValueConnectionKnob("Invoker", Direction.Out, typeof(Character))]
        public ValueConnectionKnob casterOutput;
        //Actual Variables
        Character caster;
        Coordinate castpoint;
        Dictionary<Trait, int> traits = new Dictionary<Trait, int>();
        public override void NodeGUI()
        {
            traits.Clear();
            foreach(Trait t in Enum.GetValues(typeof(Trait)))
            {
                traits.Add(t, 1);
            }
        GUILayout.BeginHorizontal();
                if(dynamicConnectionPorts.Count != traits.Count) //Redraw Values only if they arent the same
                {
                  dynamicConnectionPorts.Clear();
                    foreach (Trait t in traits.Keys)
                    {
                        ValueConnectionKnobAttribute tattr = new ValueConnectionKnobAttribute(t.ToString(), Direction.Out, typeof(float));
                        CreateValueConnectionKnob(tattr);
                        
                    }
                }
        GUILayout.EndHorizontal();
            foreach (ValueConnectionKnob t in dynamicConnectionPorts)
            {
                t.DisplayLayout();
            }
            targetpoint.DisplayLayout();
            casterOutput.DisplayLayout();
        }
        public override bool Calculate()
        {
            if(caster != null && castpoint != null)
            {
                traits = caster.traits;
            } else
            {
                castpoint = new Coordinate(1, 1, 1);
            }
            if(caster == null)
            {
                casterOutput.SetValue(null);
            } else
            {
                casterOutput.SetValue(caster);
            }
            foreach(Trait t in traits.Keys)
            {
                ValueConnectionKnob k = (ValueConnectionKnob) dynamicConnectionPorts.Find(x => (x.name == t.ToString()));
                
                k.SetValue(traits[t]);
            }
            return true;
        }

        public void Invokation(Character character, Coordinate coord)
        {
            caster = character;
            castpoint = coord;
        }
    }
}