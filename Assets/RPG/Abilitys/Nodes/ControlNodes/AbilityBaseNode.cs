﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeEditorFramework;

namespace RPGCore.Abilitys.Nodes
{
    [Node(true, "Ability/BaseNode")]
    public class AbilityBaseNode : Node
    {
        const string NodeID = "AbiltyBaseNode";
        public override string GetID { get { return NodeID; } }
        public override string Title { get { return "Ability Base Node"; } }
        public override bool AutoLayout
        {
            get
            {
                return true;
            }
        }
    }
    
}
