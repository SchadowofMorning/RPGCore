﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using NodeEditorFramework;
using RPGCore.WorldObjects;
using RPGCore.Grid;
using RPGCore.Turn;
using UnityEngine;

namespace RPGCore.Abilitys.Nodes
{
    [Node(false, "Ability/Buff", new Type[] { typeof(AbilityCanvasType) })]
    public class BuffNode : EffectBaseNode
    {
        const string NodeID = "BuffNode";
        public AbilityCanvasType ability;
        public override string GetID
        {
            get
            {
                return NodeID;
            }
        }
        public override string Title
        {
            get
            {
                return "Buff Node";
            }
        }
        [ValueConnectionKnob("Targets", Direction.In, typeof(List<Character>))]
        public ValueConnectionKnob targets;
        [ValueConnectionKnob("Value", Direction.In, typeof(float))]
        public ValueConnectionKnob Value;
        [ValueConnectionKnob("Caster", Direction.In, typeof(Character))]
        public ValueConnectionKnob CasterInput;
        [ValueConnectionKnob("Coordinate", Direction.In, typeof(List<Character>))]
        public ValueConnectionKnob TargetInput;
        [ValueConnectionKnob("Duration", Direction.In, typeof(int))]
        public ValueConnectionKnob DurationInput;
        public override void NodeGUI()
        {
            base.NodeGUI();
            ability = (AbilityCanvasType)EditorGUILayout.ObjectField(ability, typeof(AbilityCanvasType), false);
        }
        public override bool Apply()
        {
            return true;
        }
        public bool invoke()
        {
            return true;
        }
    }
}
