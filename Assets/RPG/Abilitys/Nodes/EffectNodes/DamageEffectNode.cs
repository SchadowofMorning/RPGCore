﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeEditorFramework;
using RPGCore.WorldObjects;

namespace RPGCore.Abilitys.Nodes
{
    [Node(false, "Ability/Damage Node")]
    public class DamageEffectNode : EffectBaseNode
    {
        const string NodeID = "DamageNode";
        public override string GetID
        {
            get
            {
                return NodeID;
            }
        }
        public override string Title
        {
            get
            {
                return "Damage Node";
            }
        }
        //Types
        public override bool Apply()
        {
            return true;
        }
        //Connections
        [ValueConnectionKnob("Value", Direction.In, typeof(float))]
        public ValueConnectionKnob valueInput;
        [ValueConnectionKnob("Target", Direction.In, typeof(List<Character>))]
        public ValueConnectionKnob targetInput;

        public override bool Calculate()
        {
            return true;
        }
    }
}
