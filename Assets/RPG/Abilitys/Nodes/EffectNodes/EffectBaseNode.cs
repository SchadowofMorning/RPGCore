﻿using System;
using System.Collections.Generic;
using System.Linq;
using NodeEditorFramework;
using UnityEditor;
using RPGCore.WorldObjects;
namespace RPGCore.Abilitys.Nodes
{
    [Node(true, "EffectBaseNode")]
    public abstract class EffectBaseNode : AbilityBaseNode {
        public abstract bool Apply();
    }
    
}