﻿using System;
using System.Collections.Generic;
using System.Linq;
using RPGCore.Abilitys.Nodes;
using NodeEditorFramework;
using RPGCore.Grid;
using UnityEngine;
using UnityEditor;
namespace RPGCore.Abilitys.Nodes
{
    [Node(false, "Grid/Coordinate Node")]
    public class CoordinateNode : Node
    {
        const string NodeID = "CoordinateNode";
        public override string GetID
        {
            get
            {
                return NodeID;
            }
        }
        public override string Title
        {
            get
            {
                return "Coordinate Node";
            }
        }
        public override Vector2 MinSize
        {
            get
            {
                return new Vector2(100, 80);
            }
        }
        private int x = 0;
        private int y = 0;
        private int z = 0;
        //Connections
        [ValueConnectionKnob("Input", Direction.In, typeof(Coordinate))]
        public ValueConnectionKnob coordInput;
        [ValueConnectionKnob("Output", Direction.Out, typeof(Coordinate))]
        public ValueConnectionKnob coordOutput;
      

        public override void NodeGUI()
        {
            EditorGUILayout.BeginVertical();
            x = EditorGUILayout.IntField("Pos X", x);
            y = EditorGUILayout.IntField("Pos Y", y);
            z = EditorGUILayout.IntField("Pos Z", z);
            
            EditorGUILayout.EndVertical();
            coordInput.DisplayLayout();
            coordOutput.DisplayLayout();
        }
        public override bool Calculate()
        {
            if (coordInput.IsValueNull)
            {
                coordOutput.SetValue(new Coordinate(x, y, z));
            } else
            {
                coordOutput.SetValue(coordInput.GetValue<Coordinate>() + new Coordinate(x, y, z));
            }
            return true;
        }
    }
}