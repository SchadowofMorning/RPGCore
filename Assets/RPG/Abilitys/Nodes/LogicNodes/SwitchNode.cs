﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeEditorFramework;
namespace RPGCore.LogicNodes
{
    [Node(false, "Logic/Switch")]
    class SwitchNode : Node
    {
        const string NodeID = "SwitchNode";
        public override string GetID
        {
            get
            {
                return NodeID;
            }
        }
        
        [ValueConnectionKnob("Input", Direction.In, typeof(bool))]
        public ValueConnectionKnob SignalIn;
        
        [ValueConnectionKnob("Output", Direction.Out, typeof(bool))]
        public ValueConnectionKnob SignalOut;
        
        [ValueConnectionKnob("Trigger", Direction.In, typeof(bool), NodeSide.Top)]
        public ValueConnectionKnob SignalSwitch;
        public override bool Calculate()
        {
            if(SignalIn.GetValue<bool>() && SignalSwitch.GetValue<bool>())
            {
                SignalOut.SetValue(true);
            } else
            {
                SignalOut.SetValue(false);
            }
            return true;
        }
    }
}
