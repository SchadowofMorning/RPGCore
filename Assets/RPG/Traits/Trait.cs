﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RPGCore.Traits
{
    [Serializable]
    public enum Trait { Strength, Agility, Intelligence, Durability, Dexterity, Wisdom }
}