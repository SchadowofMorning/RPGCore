﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Priority_Queue;
using RPGCore.Interfaces;
using RPGCore.WorldObjects;
using RPGCore.Grid;

namespace RPGCore.Turn
{
    class TurnManager : MonoBehaviour
    {
        private int TurnNumber;
        public delegate bool TurnEndDelegate();
        public event TurnEndDelegate TurnEnd;
        private SimplePriorityQueue<ITurn> ActiveQueue = new SimplePriorityQueue<ITurn>();
        public ITurn current { get; private set; }
        private Queue<ITurn> Done = new Queue<ITurn>();
        void Update()
        {
            current = ActiveQueue.First;
        }
        public void next()
        {
            if(ActiveQueue.Count > 0)
            {
                if(current != null)current.endTurn();
                ITurn i = ActiveQueue.Dequeue();
                Done.Enqueue(current);
                current = i;
                i.activate();
            } else
            {
                while (Done.Count > 0) {
                    ITurn i = Done.Dequeue();
                    ActiveQueue.Enqueue(i, i.speed());
                }
                TurnEnd.Invoke();
                next(); 
            }
            
        }
        public void register(ITurn obj, float f)
        {
            ActiveQueue.Enqueue(obj, f);
        }
        //Node Calculation
    }
}
