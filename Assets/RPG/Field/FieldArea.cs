﻿using System;
using System.Collections.Generic;
using RPGCore.Grid;
using RPGCore;
using UnityEngine;
using UnityEditor;

namespace RPGCore.Grid
{
    
    public class FieldArea : MonoBehaviour
    {
        [MenuItem("GameObject/RPGCore Fields/FieldArea", priority = 10)]
        static void CreateFieldArea()
        {
            GameObject fa = new GameObject();
            fa.name = "FieldArea";
            fa.AddComponent(typeof(FieldArea));
            GameObject os = new GameObject();
            os.name = "Outside";
            os.transform.parent = fa.transform;
        }
        public bool active = true;
        public List<Field> Connectors = new List<Field>();
        public List<Field> Fields = new List<Field>();
        GameObject OutsideParent;
        void Start()
        {
            foreach (Transform child in transform)
            {
                Field f = child.GetComponent<Field>();
                if (f != null)
                {
                    if (f.connector) Connectors.Add(f);
                    else Fields.Add(f);
                }
            }
            OutsideParent = transform.Find("Outside").gameObject;
        }
        void Update()
        {
            List<Field> all = new List<Field>();
            all.AddRange(Fields);
            all.AddRange(Connectors);
            foreach(Field  f in Connectors)
            {
                if (f.occupant != null) active = true;
            }
            bool trigger = false;
            foreach(Field f in all)
            {
                if(f.occupant != null)
                {
                    trigger = true;
                }
            }
            if(trigger)
            {
                active = true;
            } else
            {
                active = false;
            }
            foreach (Field f in Fields)
            {
                f.gameObject.SetActive(active);
            }
            OutsideParent.SetActive(!active);
        }
    }
}
