﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using RPGCore;
using RPGCore.Turn;
using RPGCore.Interfaces;
using RPGCore.Prompt;
using RPGCore.WorldObjects;

namespace RPGCore.Grid
{
    public class Field : MonoBehaviour, IField
    {
        [MenuItem("GameObject/RPGCore Fields/Field", priority = 11)]
        static void FieldContextMenu()
        {
            GameObject f = new GameObject();
            f.AddComponent(typeof(Field));
        }
        
        public Coordinate position = new Coordinate();
        private FieldIndex index;
        private Renderer rend;
        private Renderer frame;
        public bool connector;
        private bool selected = false;
        public Character occupant;
        private TurnManager tm;
        void Awake()
        {
            index = GameObject.Find("FieldIndex").GetComponent<FieldIndex>();
            rend = transform.Find("Base").GetComponent<Renderer>();
            position = index.register(this);
            frame = transform.Find("Frame").GetComponent<Renderer>();
            tm = GameObject.Find("TurnManager").GetComponent<TurnManager>();
        }
        public bool setFrame()
        {
            frame.material.color = rend.material.color;
            return true;
        }
        public bool setFrame(Color c)
        {
            frame.material.color = c;
            return true;
        }
        public GameObject select()
        {
            return this.gameObject;
        }

        public bool InRange(Coordinate c, int r)
        {
            bool plus = c <= position + r;
            bool minus = c >= position - r;
            return (plus && minus);
        }

        

        public Vector3 pointat()
        {
            return transform.position;
        }

        public void click()
        {
            IMove player = tm.current as IMove;
            if(player != null) if(Coordinate.inRange(player.current().position, position, player.Range()))
                {
                    WorldPrompt.ShowAt(position.GetVector3() + FieldIndex.PromptMod(), this);
                    selected = true;
                }
        }

        public Field me()
        {
            return this;
        }
        void Update()
        {
            if (selected)
            {
                setFrame(Color.green);
            } else
            {
                setFrame();
            }
        }

        public void deselect()
        {
            selected = false;
        }
    }
}
