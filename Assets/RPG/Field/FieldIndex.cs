﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using RPGCore;
using RPGCore.Interfaces;
using System.Text;

namespace RPGCore.Grid
{
    public class FieldIndex : MonoBehaviour
    {
        public Vector3 promptmodifier;
        
        public static Vector3 PromptMod()
        {
            FieldIndex fi = GameObject.Find("FieldIndex").GetComponent<FieldIndex>();
            return fi.promptmodifier;
        }
        public static Coordinate ResolveTransform(Vector3 t)
        {
            FieldIndex fi = GameObject.Find("FieldIndex").GetComponent<FieldIndex>();
            int x = (int)Mathf.Round(t.x / fi.XModifier);
            int y = (int)Mathf.Round(t.y / fi.YModifier);
            int z = (int)Mathf.Round(t.z / fi.ZModifier);
            return new Coordinate(x, y, z);
        }
        public static List<Field> getAllOccupied()
        {
            FieldIndex fi = GameObject.Find("FieldIndex").GetComponent<FieldIndex>();
            List<Field> worklist = new List<Field>();
            foreach(KeyValuePair<Coordinate, Field> kvp in fi.Fields)
            {
                if(kvp.Value.occupant != null)
                {
                    worklist.Add(kvp.Value);
                }
            }
            return worklist;
        }
        public static float[] getModifiers()
        {
            FieldIndex fi = GameObject.Find("FieldIndex").GetComponent<FieldIndex>();
            return new float[3] { fi.XModifier, fi.YModifier, fi.ZModifier}; 
        }
        public float XModifier = 1;
        public float YModifier = 1;
        public float ZModifier = 1;
        Dictionary<Coordinate, Field> Fields = new Dictionary<Coordinate, Field>();
        public Coordinate register(Field f)
        {
            int x = (int)Mathf.Round(f.transform.position.x / XModifier);
            int y = (int)Mathf.Round(f.transform.position.y / YModifier);
            int z = (int)Mathf.Round(f.transform.position.z / ZModifier);
            Coordinate c = new Coordinate(x, y, z);
            Fields.Add(c, f);
            return c;
        }
        public Field this[Coordinate c]
        {
            get
            {
                return Fields[c];
            }
            set
            {
                if (Fields[c] != null) Fields.Add(c, value);
                else Fields[c] = value;
            }
        }
    }
}
