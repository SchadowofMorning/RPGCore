﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace RPGCore.Grid
{
    
    [Serializable]
    public struct Coordinate
    {

        public int x;
        public int y;
        public int z;
        /// <summary>
        /// Used for Scaling the Tiles on the Unity grid
        /// </summary>
        /// <returns></returns>
        public Vector3 GetVector3()
        {
            float[] mods = FieldIndex.getModifiers();
            return new Vector3(mods[0] * x, mods[1] * y, mods[2] * z);
        }
        /// <summary>
        /// Base Constructor for the Grid Coordinate
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Z"></param>
        public Coordinate(int X, int Y, int Z)
        {
            x = X;
            y = Y;
            z = Z;
        }
        /// <summary>
        /// Static Function for checks, also implemented by fields
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public static bool inRange(Coordinate c1, Coordinate c2, int range)
        {
            return (c2 <= c1 + range && c2 >= c1 - range);
        }
        public static Coordinate operator +(Coordinate c1, Coordinate c2)
        {
            return new Coordinate(c1.x + c2.x, c1.y + c2.y, c1.z + c2.z);
        }
        public static Coordinate operator -(Coordinate c1, Coordinate c2)
        {
            return new Coordinate(c1.x - c2.x, c1.y - c2.y, c1.z - c2.z);
        }
        public static Coordinate operator +(Coordinate c1, int c2)
        {
            return new Coordinate(c1.x + c2, c1.y + c2, c1.z + c2);
        }
        public static Coordinate operator -(Coordinate c1, int c2)
        {
            return new Coordinate(c1.x - c2, c1.y - c2, c1.z - c2);
        }
        public static bool operator <(Coordinate c1, Coordinate c2)
        {
            bool xm = c1.x < c2.x;
            bool ym = c1.y < c2.y;
            bool zm = c1.z < c2.z;
            return (xm && ym && zm);
        }
        public static bool operator >(Coordinate c1, Coordinate c2)
        {
            bool xm = c1.x > c2.x;
            bool ym = c1.y > c2.y;
            bool zm = c1.z > c2.z;
            return (xm && ym && zm);
        }
        public static bool operator <=(Coordinate c1, Coordinate c2)
        {
            bool xm = c1.x <= c2.x;
            bool ym = c1.y <= c2.y;
            bool zm = c1.z <= c2.z;
            return (xm && ym && zm);
        }
        public static bool operator >=(Coordinate c1, Coordinate c2)
        {
            bool xm = c1.x >= c2.x;
            bool ym = c1.y >= c2.y;
            bool zm = c1.z >= c2.z;
            return (xm && ym && zm);
        }
        public static bool operator ==(Coordinate c1, Coordinate c2)
        {
            return (c1.x == c2.x && c1.y == c2.y && c1.z == c2.z);
        }
        public static bool operator !=(Coordinate c1, Coordinate c2)
        {
            return !(c1.x == c2.x && c1.y == c2.y && c1.z == c2.z);
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
