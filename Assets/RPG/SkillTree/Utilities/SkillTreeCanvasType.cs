﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeEditorFramework;
using NodeEditorFramework.Standard;
namespace RPGCore.SkillTree.Nodes
{
    [NodeCanvasType("SkillTree Canvas")]
    public class SkillTreeCanvasType : NodeCanvas
    {
        protected override void OnCreate()
        {
            Traversal = new CanvasCalculator(this);
        }
    }

}
