﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeEditorFramework;

namespace RPGCore.SkillTree.Nodes
{
    public abstract class SkillTreeBaseNode : Node
    {
        const string NodeID = "STBaseNode";
        public override string GetID
        {
            get
            {
                return NodeID;
            }
        }
    }
}
