﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeEditorFramework;
using UnityEditor;
using UnityEngine;
using RPGCore.Abilitys.Nodes;
namespace RPGCore.SkillTree.Nodes
{
    [Node(false, "SkillTree/Skill")]
    public class SkillNode : SkillTreeBaseNode
    {
        Texture2D icon;
        AbilityCanvasType skill;
        const string NodeID = "SkillNode";
        public override string GetID
        {
            get
            {
                return NodeID;
            }
        }
        public override string Title
        {
            get
            {
                return "Skill Node";
            }
        }
        public override Vector2 DefaultSize
        {
            get
            {
                return new Vector2(120, 170);
            }
        }
         
        public override bool AutoLayout
        {
            get
            {
                return false;
            }
        }
        //Connections
        [ValueConnectionKnob("DependencyOut", Direction.Out, typeof(bool), NodeSide.Top)]
        public ValueConnectionKnob DependencyOutput;
        [ValueConnectionKnob("DependencyIn", Direction.In, typeof(bool), NodeSide.Bottom)]
        public ValueConnectionKnob DependencyInput;
        
        public override bool Calculate()
        {
            return true;
        }
        public override void NodeGUI()
        {
            int IconSize = 100;
            GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.Width(IconSize), GUILayout.Height(IconSize) };
            EditorGUILayout.Space();
            icon = (Texture2D)EditorGUILayout.ObjectField(icon, typeof(Texture2D), false, options);
            DependencyInput.sidePosition = 50;
            DependencyOutput.sidePosition = 50;
            skill = (AbilityCanvasType) EditorGUILayout.ObjectField(skill, typeof(AbilityCanvasType), false);
        }
    }
}
