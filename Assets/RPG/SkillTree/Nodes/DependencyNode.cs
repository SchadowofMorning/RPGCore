﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeEditorFramework;
using UnityEditor;
using UnityEngine;
namespace RPGCore.SkillTree.Nodes
{
   [Node(false, "SkillTree/Dependency Node")]
    public class DependencyNode : Node
    {
        const string NodeID = "DependencyNode";
        public override string GetID
        {
            get
            {
                return NodeID;
            }
        }
        public override string Title
        {
            get
            {
                return "Dependency Node";
            }
        }
        private int depCount;
        [ValueConnectionKnob("Output", Direction.Out, typeof(bool), NodeSide.Top)]
        public ValueConnectionKnob validOutput;
        
        public override void NodeGUI()
        {
            validOutput.sidePosition = 100;
            ValueConnectionKnobAttribute dynoAttr = new ValueConnectionKnobAttribute("Dependency", Direction.In, typeof(bool), NodeSide.Bottom);
            if (dynamicConnectionPorts.Count != depCount)
            {
                dynamicConnectionPorts.Clear();
                for (int i = 0; i < depCount; i++)
                {
                    CreateValueConnectionKnob(dynoAttr);
                }
            }
            int dynocount = 0;
            foreach(ValueConnectionKnob k in dynamicConnectionPorts)
            {
                k.sidePosition = (200 / dynamicConnectionPorts.Count) * dynocount;
                dynocount++;
            }
            dynocount = 0;
            if (GUILayout.Button("+"))
            {
                depCount++;
            }
            if (GUILayout.Button("-"))
            {
                if(depCount != 0) depCount--;
            }
        }
    }
    
}
