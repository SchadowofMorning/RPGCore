﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCore.SkillTree
{
    public struct SkillDependency
    {
        Dictionary<Skill, int> list;
        public SkillDependency(Dictionary<Skill, int> li)
        {
            list = li;
        }
        public bool fullfilled(Dictionary<Skill, int> skilllist)
        {
            bool flip = true;
            if (list.Count == 0) return flip;
            foreach(KeyValuePair<Skill, int> kvp in list)
            {
                if (skilllist.ContainsKey(kvp.Key))
                {
                    int lvl = skilllist[kvp.Key];
                    if(!(lvl >= kvp.Value))
                    {
                        flip = false;
                    }
                } else
                {
                    flip = false;
                }
                
            }
            return flip;
        }
    }
}
