﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RPGCore.SkillTree
{
    public class SkillTree : ScriptableObject
    {
        Dictionary<int, Skill> skills = new Dictionary<int, Skill>();
        Dictionary<int, SkillDependency> dependencys = new Dictionary<int, SkillDependency>();
        Dictionary<int, int> level = new Dictionary<int, int>();
        public SkillTuple this[int index] {
            get
            {
                return new SkillTuple(skills[index], dependencys[index], level[index]);
            }
            set
            {
                overrideEntry<Skill>(ref skills, value.skill);
                overrideEntry<SkillDependency>(ref dependencys, value.dependency);
                level[index] = value.level;
            }
        }
        private void overrideEntry<T>(ref Dictionary<int, T> dic, T val)
        {
            if (dic.ContainsValue(val))
            {
                KeyValuePair<int, T> kvp = dic.Single(x => (x.Value.Equals(val)));
                dic[kvp.Key] = val;
            } else
            {
                dic.Add(dic.Count, val);
            }
        }
    }
    public struct SkillTuple
    {
        public Skill skill;
        public SkillDependency dependency;
        public int level;
        public SkillTuple(Skill sk, SkillDependency dep, int lvl)
        {
            skill = sk;
            dependency = dep;
            level = lvl;
        }
    }
}
