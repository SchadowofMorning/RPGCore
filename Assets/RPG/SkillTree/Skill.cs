﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using RPGCore.Abilitys.Nodes;
namespace RPGCore.SkillTree
{
    public class Skill : ScriptableObject
    {
        public Texture2D icon;
        public AbilityCanvasType logic;
    }
}
