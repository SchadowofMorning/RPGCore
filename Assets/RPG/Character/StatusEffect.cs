﻿using UnityEngine;
using RPGCore.Interfaces;
using System;
using RPGCore.Turn;
namespace RPGCore.Traits
{
    public class StatusEffect : ITick
    {
        public Trait trait;
        public int value;
        public int timer { get; private set; }
        public bool active  { get; private set; }
        public StatusEffect(Trait t, int v, int ti)
        {
            trait = t;
            value = v;
            timer = ti;
            active = true;
            TurnManager tm = GameObject.Find("TurnManager").GetComponent<TurnManager>();
            tm.TurnEnd += decay;
        }
        public bool decay()
        {
            timer--;
            if (timer == 0) active = false;
            return active;
        }
    }
}