﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPGCore.WorldObjects;
using RPGCore.Interfaces;
using System;

public class Player : Character, IControllable {
    public bool myTurn()
    {
        return true;
    }

    protected override void Start () {
        base.Start();
	}
	
	// Update is called once per frame
	protected override void Update () {
        base.Update();
	}
}
