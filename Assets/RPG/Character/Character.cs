﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPGCore.Traits;
using RPGCore.Turn;
using RPGCore.Interfaces;
using System;
using RPGCore.Grid;

namespace RPGCore.WorldObjects
{
    public class Character : MonoBehaviour, ITurn, IMove, ISelectable
    {
        #region GameRelated definitions
        public Dictionary<Trait, int> traits;
        public List<StatusEffect> modifier;
        #endregion
        private TurnManager manager;
        public Field field;
        bool active;
        [SerializeField]
        private Vector3 HeightMod;
        protected virtual void Start()
        {
            manager = GameObject.Find("TurnManager").GetComponent<TurnManager>();
            register();
            if (field == null) field = underMe();
        }

        // Update is called once per frame
        protected virtual void Update()
        {

        }
        
        protected Field underMe()
        {
            Vector3 Down = new Vector3(0, -5, 0) + transform.position;
            RaycastHit hit;
            if(Physics.Raycast(transform.position, Down, out hit))
            {
                GameObject gf = hit.transform.gameObject;
                Field f = gf.GetComponent<Field>();
                if(f != null)
                {
                    return f;
                } else
                {
                    return null;
                }
                
            } else
            {
                return null;
            }
        }
        #region Coroutines
        Vector3 goal;
        protected virtual IEnumerator Movement()
        {

            while (transform.position != goal)
            {
                transform.position = Vector3.Lerp(transform.position, goal, 3 * Time.deltaTime);
                yield return null;
            }
        }
        #endregion
        #region Interface implementations
        public void activate()
        {
            active = true;
        }

        public virtual float speed()
        {
            return 1;
        }

        public void register()
        {
            manager.register(this, speed());
        }

        public void endTurn()
        {
            active = false;
        }

        public GameObject select()
        {
            return gameObject;
        }

        public Vector3 pointat()
        {
            return transform.position;
        }

        public bool InRange(Coordinate c, int r)
        {
            return (field.position <= c + r && field.position >= c - r) ? true : false;
        }

        public void click()
        {
            throw new NotImplementedException();
        }
        public int Range()
        {
            return 2;
        }
        public Field moveTo(Field f)
        {
            field.occupant = null;
            this.field = f;
            f.occupant = this;
            goal = field.transform.position + HeightMod;
            StartCoroutine("Movement");
            return f;
        }

        public Field teleportTo(Field f)
        {
            transform.position = f.transform.position + HeightMod;
            this.field = f;
            return f;
        }

        public Field current()
        {
            return field;
        }


        #endregion
    }
}
