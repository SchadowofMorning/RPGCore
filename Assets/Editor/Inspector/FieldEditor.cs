﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using RPGCore.Grid;

namespace RPGCoreEditor
{
    [CustomEditor(typeof(Field))]
    public class FieldEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            Field f = (Field)target;
            if(GUILayout.Button("Fix Position"))
            {
                f.transform.position = f.position.GetVector3();
            }
        }
    }
}