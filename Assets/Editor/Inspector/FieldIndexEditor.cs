﻿using System;
using System.Collections.Generic;
using RPGCore.Grid;
using UnityEngine;
using UnityEditor;

namespace RPGCoreEditor
{
    [CustomEditor(typeof(FieldIndex))]
    class FieldIndexEditor : Editor
    {
        GameObject[] fields;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if(GUILayout.Button("Fix all Fields"))
            {
                fields = GameObject.FindGameObjectsWithTag("Field");
                foreach(GameObject g in fields)
                {
                    Field f = g.GetComponent<Field>();
                    f.transform.position = f.position.GetVector3();
                }
            }
        }
    }
}
