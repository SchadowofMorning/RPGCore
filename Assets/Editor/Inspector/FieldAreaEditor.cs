﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using RPGCore.Grid;

namespace RPGCoreEditor
{
    [CustomEditor(typeof(FieldArea))]
    class FieldAreaEditor : Editor
    {
        int x = 0;
        int y = 0;
        int z = 0;
        int range = 0;
        int cutx = 0;
        int cutz = 0;
        Field Templatefield;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            EditorGUILayout.LabelField("Fill Field Area");
            x = EditorGUILayout.IntField("Center X", x);
            y = EditorGUILayout.IntField("Center Y", y);
            z = EditorGUILayout.IntField("Center Z", z);
            
            range = EditorGUILayout.IntField("Range", range);
            
            cutx = EditorGUILayout.IntField("Cut down X", cutx);
            
            cutz = EditorGUILayout.IntField("Cut down z", cutz);
            if (GUILayout.Button("Fill area and set position"))
            {
                GameObject Templatefield = (GameObject)Resources.Load("Field");
                int startx = x - range;
                int startz = z - range;
                int endx = x + range;
                int endz = z + range;
                for(int cx = 0; cx < Mathf.Abs(endx - startx); cx++)
                {
                    for (int cz = 0; cz < Mathf.Abs(endz - startz); cz++)
                    {
                        Field f = Instantiate(Templatefield).GetComponent<Field>();
                        f.position = new Coordinate(cx + startx, y, cz + startz);
                        f.transform.position = f.position.GetVector3();
                        f.name = "Field(" + cx.ToString() + "|" + cz.ToString() + ")";
                        f.transform.parent = Selection.activeGameObject.transform;
                    }
                }
            }
            if(GUILayout.Button("Fix All fields current coordinates"))
            {
                foreach(Transform child in Selection.activeGameObject.transform)
                {
                    Field f = child.GetComponent<Field>();
                    if(f != null)
                    {
                        f.position = FieldIndex.ResolveTransform(child.position);
                    }
                }
            }
        }
    }
}
